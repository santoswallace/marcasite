<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{   
    use SoftDeletes;
    protected $appends = [
        'validate_course_end',
        'validate_max_subscriptions',
        'number_inscriptions'
    ];
    
    protected $fillable = [
        'user_id',
        'name',
        'description',
        'value',
        'start',
        'end',
        'max_subscriptions',
        'material'
    ];

    public function author(){
        return $this->belongsTo(User::class);
    }

    public function inscriptions(){
        return $this->hasMany(Inscription::class);
    }

    public function getValidateCourseEndAttribute()
    {
        return $this->end <= Carbon::now();
    }

    public function getValidateMaxSubscriptionsAttribute()
    {
        return $this->inscriptions()->count() == $this->max_subscriptions;
    }

    public function getNumberInscriptionsAttribute()
    {
        return $this->inscriptions()->count();
    }
}
