<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InscriptionType extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name'
    ];

    public function courses()
    {
       return $this->hasMany(Inscription::class);
    }

}
