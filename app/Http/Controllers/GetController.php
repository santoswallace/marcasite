<?php

namespace App\Http\Controllers;

use App\Course;
use App\User;
use Illuminate\Http\Request;

class GetController extends Controller
{
    public function getUser($id){
        return  User::select('id', 'cpf','name','email')->find($id);
    }

    public function getCourses($id){

        //Poderia fazer a validação pra pegar apenas os que ainda estao em aberto, 
        //não vou fazer, pra vermos a validação na tela.
        return Course::find($id);
    }
}
