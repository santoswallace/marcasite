<?php

namespace App\Http\Controllers;

use App\TypeUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('type_user')->get();
        $types = TypeUser::all();
        return view('app.user.index', compact('users', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed',
            'cpf' => 'required|unique:users',
            'address' => 'required',
            'company' => 'required',
            'phone' => 'required',
            'mobile' => 'required',
            'type_user_id' => 'required|int',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $user =  User::create($request->all());
        Log::info('User create ID: ' . $user->id);

        return response()->json(['id' =>  $user->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        if (!$user) {
            return redirect()->route('user.index');
        }

        $types = TypeUser::all();
        return view('app.user.edit', compact('user', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect()->route('user.index');
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $user->id,
            'cpf' => 'required|unique:users,cpf,' . $user->id,
            'address' => 'required',
            'company' => 'required',
            'phone' => 'required',
            'mobile' => 'required',
            'type_user_id' => 'required|int',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        if(isset($request->password)){
            $validator = Validator::make($request->all(), [
                'password' =>  'confirmed'
            ]);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()]);
            }

            $request['password'] = Hash::make($request->password);

        }else{
            unset($request['password']);
        }

        $user->update($request->all());
        $user->save();
        
        Log::info('User Update ID: ' . $user->id);
        return response()->json(['id' =>  $user->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return response()->json(['status' => true]);
    }
}
