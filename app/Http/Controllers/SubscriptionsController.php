<?php

namespace App\Http\Controllers;

use App\Course;
use App\Inscription;
use App\InscriptionType;
use App\Status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SubscriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inscriptions = Inscription::with('course', 'user', 'inscription_type', 'status')
            ->withTrashed()->get();
        $users = User::all();
        $courses = Course::all();
        $types = InscriptionType::all();
        $statuses = Status::all();
        return view('app.subscription.index', compact('users', 'courses', 'types', 'inscriptions', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'course_id' => 'required',
            'inscription_type_id' => 'required',
            'status_id' => 'required',
        ], [
            'user_id.required' => "Este campo é obrigatório",
            'course_id.required' => "Este campo é obrigatório",
            'inscription_type_id.required' => "Este campo é obrigatório",
            'status_id.required' => "Este campo é obrigatório"
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $course = Course::find($request->course_id);
        if ($course->validate_course_end) {
            return response()->json(['errors' => ['course_id' => ['Matriculas já encerradas']]]);
        }

        if($course->validate_max_subscriptions){
            return response()->json(['errors' => ['course_id' => ['Este Curso já atingiu o limite']]]);
        }

        $inscription = Inscription::whereUserId($request->user_id)->whereCourseId($request->course_id)->first();
        if($inscription){
            return response()->json(['errors' => ['course_id' => ['Este usuário já esta inscrito']]]);
        }

        $inscription = Inscription::create($request->all());
        Log::info('Create Inscription: ' . json_encode($request->all()));
        return response()->json(['id' => $inscription->id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'course_id' => 'required',
            'inscription_type_id' => 'required',
            'status_id' => 'required',
        ], [
            'user_id.required' => "Este campo é obrigatório",
            'course_id.required' => "Este campo é obrigatório",
            'inscription_type_id.required' => "Este campo é obrigatório",
            'status_id.required' => "Este campo é obrigatório"
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $course = Course::find($request->course_id);
        if ($course->validate_course_end) {
            return response()->json(['errors' => ['course_id' => ['Matriculas já encerradas']]]);
        }

        if($course->validate_max_subscriptions){
            return response()->json(['errors' => ['course_id' => ['Este Curso já atingiu o limite']]]);
        }

        $inscription = Inscription::where('id', '!=', $id)->whereUserId($request->user_id)->whereCourseId($request->course_id)->first();
        if($inscription){
            return response()->json(['errors' => ['course_id' => ['Este usuário já esta inscrito']]]);
        }

        $inscription = Inscription::find($id);
        $inscription->course_id = $request->course_id;
        $inscription->inscription_type_id = $request->inscription_type_id;
        $inscription->user_id = $request->user_id;
        $inscription->status_id = $request->status_id;
        $inscription->save();

        Log::info('Update Inscription: ' . json_encode($request->all()));
        return response()->json(['id' => $inscription->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Inscription::find($id)->delete();
        return response()->json(['status' => true]);
    }
}
