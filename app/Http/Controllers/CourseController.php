<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $courses = Course::all();
        return view('app.course.index', compact('courses'));
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request['user_id'] = Auth::user()->id;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'material' => 'required|url',
            'value' => 'required',
            'start' => 'required',
            'end' => 'required',
            'max_subscriptions' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        if($request->end < $request->start){
            return response()->json(['errors' => ['end' => ['Sua data de finalização não pode ser menor que o inicio']] ]);
        }

        //Poderia usar um transaction
        $course = Course::create($request->all());
        Log::info('Course Create ID: ' . $course->id);
        return response()->json(['id' =>  $course->id]);
    }

  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::find($id);
        return view('app.course.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['user_id'] = Auth::user()->id;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'material' => 'required|url',
            'value' => 'required',
            'start' => 'required',
            'end' => 'required',
            'max_subscriptions' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        if($request->end < $request->start){
            return response()->json(['errors' => ['end' => ['Sua data de finalização não pode ser menor que o inicio']] ]);
        }

        //Poderia usar um transaction
        $course = Course::find($id)->update($request->all());
        Log::info('Course Create ID: ' . $id);
        return response()->json(['id' =>  $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Course::find($id)->delete();
        return response()->json(['status' => true]);
    }
}
