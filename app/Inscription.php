<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inscription extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'course_id',
        'inscription_type_id',
        'user_id',
        'status_id',
    ];

    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function inscription_type(){
        return $this->belongsTo(InscriptionType::class);
    }

    public function status(){
        return $this->belongsTo(Status::class);
    }
}

