<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'color'
    ];

    public function inscriptions(){
        return $this->hasMany(Inscription::class);
    }
}
