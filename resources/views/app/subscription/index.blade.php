@extends('layouts.app')
@section('content')

<form-subscription :statuses = "{{ $statuses }}" :users = "{{ $users }}" :courses="{{ $courses }}" :types="{{ $types }}"></form-subscription>
<list-subscription :inscriptions = "{{ $inscriptions }}" :statuses = "{{ $statuses }}" :users = "{{ $users }}" :courses="{{ $courses }}" :types="{{ $types }}"></list-subscription>
    
@endsection