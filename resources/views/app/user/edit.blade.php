@extends('layouts.app')
@section('content')
<edit-user :edit="{{ $user }}" :users-type="{{ $types }}"></edit-user>
@endsection