@extends('layouts.app')
@section('content')
<form-user  :users-type="{{ $types }}" ></form-user>
<list-user  :users="{{ $users ?? null }}" :active-user="{{ Auth::user() ?? null }}"></list-user>
@endsection