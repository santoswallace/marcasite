@extends('layouts.app')
@section('content')
<form-course></form-course>
<list-course :courses="{{ $courses }}" :active-user="{{ Auth::user() ?? null }}"></list-course>
@endsection