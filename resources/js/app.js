import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import VueMask from 'v-mask'
import vSelect from "vue-select";
import "vue-select/dist/vue-select.css";



require('./bootstrap');
require('datatables.net-bs4')

window.Vue = require('vue');

Vue.use(VueToast);
Vue.use(VueMask);
Vue.component("v-select", vSelect);

const app = new Vue({
    el: '#app',
    components:{
        Dashboard: () => import('./components/dashboard/Dashboard'),
        FormUser: () => import('./components/user/Form'),
        ListUser: () => import('./components/user/List'),
        EditUser: () => import('./components/user/Edit'),

        FormCourse: () => import('./components/course/Form'),
        ListCourse: () => import('./components/course/List'),
        EditCourse: () => import('./components/course/Edit'),

        FormSubscription: () => import('./components/subscription/Form'),
        ListSubscription: () => import('./components/subscription/List'),

    }
});
