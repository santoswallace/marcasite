<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();
Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');
Route::resource('user', 'UserController')->except(['show', 'create']);
Route::resource('course', 'CourseController')->except(['show', 'create']);
Route::resource('subscription', 'SubscriptionsController')->except(['show', 'create']);


Route::get('getuser/{cpf}', 'GetController@getUser')->name('get.user'); 
Route::get('getcourses/{id?}', 'GetController@getCourses')->name('get.courses'); 
