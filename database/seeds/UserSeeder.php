<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate(
            ['id' => 1],
            [
                'id' => 1,
                'type_user_id' => 1,
                'name' => 'Wallace',
                'email' => 'swallace@outlook.com.br',
                'cpf' => '441.222.587-60',
                'address' => "Rua das Flores, 12, Morumbi, São Paulo",
                'company' => 'WSANTOS PROGRAMMER',
                'phone' => '(11) 4401-8878',
                'mobile' => '(11) 98277-6144',
                'password' => Hash::make('123456')
            ]
        );
    }
}
