<?php

use App\TypeUser;
use Illuminate\Database\Seeder;

class TypeUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeUser::updateOrCreate(
            ['id' => 1],
            [
                'id' => 1,
                'name' => 'Root'
            ]
        );

        TypeUser::updateOrCreate(
            ['id' => 2],
            [
                'id' => 2,
                'name' => 'Professor'
            ]
        );

        TypeUser::updateOrCreate(
            ['id' => 3],
            [
                'id' => 3,
                'name' => 'Aluno'
            ]
        );
    }
}
