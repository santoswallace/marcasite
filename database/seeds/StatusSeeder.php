<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::updateOrcreate(
            ['id' => 1],
            [
                'id' => 1,
                'name' => 'Aguardando Pagamento',
                'color' => 'yellow',
            ]
        );

        Status::updateOrcreate(
            ['id' => 2],
            [
                'id' => 2,
                'name' => 'Pagamento Confirmado',
                'color' => 'blue',
            ]
        );

        Status::updateOrcreate(
            ['id' => 3],
            [
                'id' => 3,
                'name' => 'Boleto Gerado',
                'color' => 'orange',
            ]
        );

        Status::updateOrcreate(
            ['id' => 4],
            [
                'id' => 4,
                'name' => 'Cancelado',
                'color' => 'red',
            ]
        );
    }
}
