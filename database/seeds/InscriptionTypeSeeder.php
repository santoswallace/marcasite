<?php

use App\InscriptionType;
use Illuminate\Database\Seeder;

class InscriptionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InscriptionType::updateOrCreate(
            ['id' => 1],
            [
                'id' => 1,
                'name' => 'Estudante'
            ]
        );

        InscriptionType::updateOrCreate(
            ['id' => 2],
            [
                'id' => 2,
                'name' => 'Profissional'
            ]
        );

        InscriptionType::updateOrCreate(
            ['id' => 3],
            [
                'id' => 3,
                'name' => 'Associado'
            ]
        );
    }
}
